FROM ghcr.io/graalvm/jdk-community:22.0.2
ENV HELM_VERSION=v3.17.1
ENV NODE_VERSION=22.x
RUN microdnf module disable -y nodejs \
    && curl -sL https://rpm.nodesource.com/setup_${NODE_VERSION} | bash - \
    && curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo \
    && curl -sL https://download.docker.com/linux/centos/docker-ce.repo | tee /etc/yum.repos.d/docker.repo \
    && microdnf install nodejs yarn docker-ce-cli git xorg-x11-server-Xvfb gtk3-devel nss alsa-lib
RUN curl -sSL https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar xz -C /tmp \
    && mv /tmp/linux-amd64/helm /usr/local/bin/helm \
    && chmod +x /usr/local/bin/helm \
    && rm -rf /tmp/linux-amd64
